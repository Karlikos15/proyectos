C:\Users\A10\Desktop>cd \

C:\>c:

C:\>cd C:\Windows\System32

C:\Windows\System32>NET START    "OracleVssWriterORCL"
El servicio de Oracle ORCL VSS Writer Service está iniciándose.
El servicio de Oracle ORCL VSS Writer Service se ha iniciado correctamente.


C:\Windows\System32>NET START    "OracleDBConsoleorcl"
El servicio de OracleDBConsoleorcl está iniciándose...............
El servicio de OracleDBConsoleorcl no ha podido iniciarse.

Error específico del servicio: 2.

Puede obtener más ayuda con el comando NET HELPMSG 3547.


C:\Windows\System32>NET START    "OracleJobSchedulerORCL"
El servicio de OracleJobSchedulerORCL está iniciándose.
El servicio de OracleJobSchedulerORCL se ha iniciado correctamente.


C:\Windows\System32>NET START    "OracleOraDb11g_home1TNSListener"
El servicio de OracleOraDb11g_home1TNSListener está iniciándose.
El servicio de OracleOraDb11g_home1TNSListener se ha iniciado correctamente.


C:\Windows\System32>NET START    "OracleServiceORCL"
El servicio de OracleServiceORCL está iniciándose...............................
..................................
El servicio de OracleServiceORCL se ha iniciado correctamente.


C:\Windows\System32>cd C:\app\A\product\11.1.0\client_1\BIN
El sistema no puede encontrar la ruta especificada.

C:\Windows\System32>sqlplus system/system @D:\file1.sql

SQL*Plus: Release 11.1.0.6.0 - Production on MiÚ Dic 7 08:13:53 2016

Copyright (c) 1982, 2007, Oracle.  All rights reserved.


Conectado a:
Oracle Database 11g Enterprise Edition Release 11.1.0.6.0 - Production
With the Partitioning, OLAP, Data Mining and Real Application Testing options

SP2-0310: no se ha podido abrir el archivo "D:\file1.sql"
SQL>
SQL> connect;
Introduzca el nombre de usuario: system
Introduzca la contrase±a:
Conectado.
SQL> CREATE SEQUENCE SEQ_ID_ARTIST
  2  START WITH 1
  3  INCREMENT BY 1;

Secuencia creada.

SQL>
SQL> CREATE TABLE artist
  2  (
  3     ID_ARTIST NUMBER(4),
  4     NAME VARCHAR2(50),
  5     SURNAME VARCHAR2(50),
  6     NATIONALITY VARCHAR2(50)
  7  );

Tabla creada.

SQL>
SQL> INSERT INTO artist (ID_ARTIST, NAME, SURNAME, NATIONALITY)
  2  VALUES (SEQ_ID_ARTIST.NEXTVAL, 'Laura', 'Garcia', 'Espana');

1 fila creada.

SQL>
SQL>
SQL> CREATE OR REPLACE PACKAGE artists IS
  2  TYPE vCursor IS REF CURSOR;
  3
  4  PROCEDURE existsArtist
  5  (
  6     vName IN artist.NAME%TYPE,
  7     vSurname IN artist.SURNAME%TYPE,
  8     vNationality IN artist.NATIONALITY%TYPE,
  9     vUserCursor OUT vCursor
 10  );
 11
 12  PROCEDURE findAll
 13  (
 14     vUserCursor OUT vCursor
 15  );
 16
 17  PROCEDURE insertArtist
 18  (
 19     vName IN artist.NAME%TYPE,
 20     vSurname IN artist.SURNAME%TYPE,
 21     vNationality IN artist.NATIONALITY%TYPE,
 22     rowCount OUT INTEGER
 23  );
 24
 25  PROCEDURE updateArtist
 26  (
 27     vIdArtist IN artist.ID_ARTIST%TYPE,
 28     vName IN artist.NAME%TYPE,
 29     vSurname IN artist.SURNAME%TYPE,
 30     vNationality IN artist.NATIONALITY%TYPE
 31  );
 32
 33  PROCEDURE deleteArtist
 34  (
 35     vIdArtist IN artist.ID_ARTIST%TYPE
 36  );
 37
 38  END artists;
 39  /

Paquete creado.

SQL>
SQL>
SQL> CREATE OR REPLACE PACKAGE BODY artists IS
  2     PROCEDURE existsArtist
  3     (
  4             vName IN artist.NAME%TYPE,
  5             vSurname IN artist.SURNAME%TYPE,
  6             vNationality IN artist.NATIONALITY%TYPE,
  7             vUserCursor OUT vCursor
  8     ) IS
  9     BEGIN
 10             OPEN vUserCursor FOR
 11                     SELECT * FROM artist
 12                     WHERE NAME = vName AND SURNAME = vSurname AND NATIONALIT
Y = vNationality;
 13     END existsArtist;
 14
 15
 16     PROCEDURE findAll
 17     (
 18             vUserCursor OUT vCursor
 19     )IS
 20     BEGIN
 21             OPEN vUserCursor FOR
 22                     SELECT * FROM artist;
 23     END findAll;
 24
 25
 26     PROCEDURE insertArtist
 27     (
 28             vName IN artist.NAME%TYPE,
 29             vSurname IN artist.SURNAME%TYPE,
 30             vNationality IN artist.NATIONALITY%TYPE,
 31             rowCount OUT INTEGER
 32     ) IS
 33     BEGIN
 34             INSERT INTO artist VALUES (SEQ_ID_ARTIST.NEXTVAL, vName, vSurnam
e, vNationality);
 35             rowCount:=SQL%ROWCOUNT;
 36     END insertArtist;
 37
 38
 39     PROCEDURE updateArtist
 40     (
 41             vIdArtist IN artist.ID_ARTIST%TYPE,
 42             vName IN artist.NAME%TYPE,
 43             vSurname IN artist.SURNAME%TYPE,
 44             vNationality IN artist.NATIONALITY%TYPE
 45     ) IS
 46     BEGIN
 47             UPDATE artist SET NAME=vName, SURNAME=vSurname, NATIONALITY=vNat
ionality
 48             WHERE ID_ARTIST=vIdArtist;
 49     END updateArtist;
 50
 51
 52     PROCEDURE deleteArtist
 53     (
 54             vIdArtist IN artist.ID_ARTIST%TYPE
 55     ) IS
 56     BEGIN
 57             DELETE FROM artist WHERE ID_ARTIST=vIdArtist;
 58     END deleteArtist;
 59  END artists;
 60  /

Cuerpo del paquete creado.