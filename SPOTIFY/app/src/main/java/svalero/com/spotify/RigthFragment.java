package svalero.com.spotify;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import svalero.com.spotify.adapter.AdapterList;
import svalero.com.spotify.beans.Artist;
import svalero.com.spotify.data.ArtistData;

public class RigthFragment extends Fragment {
    private OnFragmentInteractionListener mListener;

    public RigthFragment() {
        // Required empty public constructor
    }

    public static RigthFragment newInstance(Bundle params) {
        RigthFragment fragment = new RigthFragment();

        if (params != null) {
            fragment.setArguments(params);
        }
        return fragment;
    }

    public static RigthFragment newInstance() {
        return newInstance(null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnFragmentInteractionListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private ListView lstArtist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_rigth, container, false);

        if (view != null) {
            lstArtist = (ListView) view.findViewById(R.id.artistList);
        }
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArtistData artistData = new ArtistData();
        artistData.initArtistList();
        lstArtist.setAdapter(new AdapterList(this, artistData.getLstArtist()));
        lstArtist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Artist artist);
    }
}
