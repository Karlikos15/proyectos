package svalero.com.spotify;

import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

import svalero.com.spotify.beans.Artist;
import svalero.com.spotify.tools.Post;


public class MainActivity extends AppCompatActivity implements RigthFragment.OnFragmentInteractionListener {
    private Button btnInsert;
    private Button btnListar;
    private RigthFragment rigthFragment;
    private EditText edtName;
    private EditText edtSurname;
    private EditText edtNationality;

    private String URL = "http://192.168.56.1:8080/SpotifyArtistsController/Controller";

    private static MainActivity mainActivity;

    public static MainActivity getInstance() {
        return mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainActivity = this;



        btnInsert = (Button) findViewById(R.id.btnInsert);

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtName = (EditText) findViewById(R.id.nameArtist);
                edtSurname = (EditText) findViewById(R.id.surnameArtist);
                edtNationality= (EditText) findViewById(R.id.nationalityArtist);

                HashMap<String, String> parametros = new HashMap<String, String>();
                parametros.put("ACTION", "ARTIST.ADD");
                parametros.put("NAME", edtName.getText().toString());
                parametros.put("SURNAME", edtSurname.getText().toString());
                parametros.put("NATIONALITY", edtNationality.getText().toString());

                TareaSegundoPlano tarea = new TareaSegundoPlano(parametros);
                tarea.execute(URL);
            }
        });

        btnListar= (Button) findViewById(R.id.btnListar);

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //CARGA ARTISTA
                HashMap<String, String> parametros = new HashMap<String, String>();
                parametros.put("ACTION", "ARTIST.LIST");

                TareaSegundoPlano tarea = new TareaSegundoPlano(parametros);
                tarea.execute(URL);

                rigthFragment = RigthFragment.newInstance();
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.rightFragment, rigthFragment);
                ft.commit();
                //FIN CARGA ARTISTA
            }
        });
    }

    @Override
    public void onFragmentInteraction(Artist artist) {
        edtName.setText(artist.getName());
        edtSurname.setText(artist.getSurname());
        edtNationality.setText(artist.getNationality());
    }

    class TareaSegundoPlano extends AsyncTask<String, Integer, Boolean> {
        private HashMap<String, String> parametros = null;
        private ArrayList<Artist> lstArtist = null;

        public TareaSegundoPlano(HashMap<String, String> parametros) {
            this.parametros = parametros;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String url_select = params[0];
            try {
                Post post = new Post();
                JSONArray result = post.getServerDataPost(parametros, url_select);
                lstArtist = Artist.getArrayListFromJSon(result);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Boolean resp) {
        }
    }
}
