package svalero.com.spotify.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

import svalero.com.spotify.R;
import svalero.com.spotify.beans.Artist;
import svalero.com.spotify.RigthFragment;
import svalero.com.spotify.tools.Post;

public class AdapterList extends ArrayAdapter<Artist> {
    private ArrayList<Artist> artists;
    private RigthFragment rigthFragment;

    private String URL = "http://192.168.56.1:8080/SpotifyArtistsController/Controller";

    public AdapterList(RigthFragment rigthFragment, ArrayList<Artist> artists) {
        super((Context) rigthFragment.getActivity(), R.layout.item_list, artists);
        this.artists = artists;
        this.rigthFragment = rigthFragment;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = rigthFragment.getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.item_list, null);

        Artist artist = artists.get(position);

        TextView textView = (TextView) view.findViewById(R.id.artistNameSurname);
        textView.setText(artist.getName());

        //LISTENER BOTONES

        return view;
    }

    class TareaSegundoPlano extends AsyncTask<String, Integer, Boolean> {

        private HashMap<String, String> parametros = null;
        private ArrayList<Artist> lstArtist=null;

        public TareaSegundoPlano(HashMap<String, String> parametros) {
            this.parametros = parametros;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String url_select = params[0];
            try {
                Post post = new Post();
                JSONArray result = post.getServerDataPost(parametros, url_select);
                lstArtist = Artist.getArrayListFromJSon(result);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Boolean resp) {
        }
    }
}
