package svalero.com.spotify.data;

import android.os.AsyncTask;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

import svalero.com.spotify.beans.Artist;
import svalero.com.spotify.tools.Post;

public class ArtistData {
    private static ArrayList<Artist> lstArtist = new ArrayList<>();

    private static Artist artist;


    public static Artist getArtist() {
        return artist;
    }

    public static void setArtist(Artist artist) {
        ArtistData.artist = artist;
    }

    public static ArrayList<Artist> getLstArtist() {
        return lstArtist;
    }

    private String URL = "http://192.168.56.1:8080/SpotifyArtistsController/Controller";

    public void initArtistList() {
        HashMap<String, String> parametros = new HashMap<>();
        parametros.put("ACTION", "ARTIST.LIST");

        TareaSegundoPlano tarea = new TareaSegundoPlano(parametros);

        tarea.execute(URL);
    }

    class TareaSegundoPlano extends AsyncTask<String, Integer, Boolean> {

        private HashMap<String, String> parametros = null;
        private ArrayList<Artist> lstArtist = null;

        public TareaSegundoPlano(HashMap<String, String> parametros) {
            this.parametros = parametros;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String url_select = params[0];
            try {
                Post post = new Post();
                JSONArray result = post.getServerDataPost(parametros, url_select);
                lstArtist = Artist.getArrayListFromJSon(result);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(Boolean resp) {
        }
    }
}
