package svalero.com.spotify.beans;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Artist {
    private int idArtist;
    private String name;
    private String surname;
    private String nationality;

    private final static String ID_ARTIST = "ID_ARTIST";
    private final static String NAME = "NAME";
    private final static String SURNAME = "SURNAME";
    private final static String NATIONALITY = "NATIONALITY";

    public Artist(int idArtist, String name, String surname, String nationality) {
        this.idArtist = idArtist;
        this.name = name;
        this.surname = surname;
        this.nationality = nationality;
    }

    public Artist(String name, String surname, String nationality) {
        this.name = name;
        this.surname = surname;
        this.nationality = nationality;
    }

    public Artist(int idArtist) {
        this.idArtist = idArtist;
    }

    public Artist() {
    }

    public int getIdArtist() {
        return idArtist;
    }

    public void setIdArtist(int idArtist) {
        this.idArtist = idArtist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public static ArrayList<Artist> getArrayListFromJSon(JSONArray datos) {
        ArrayList<Artist> tableArtists = null;
        Artist artist = null;
        try {
            if (datos != null && datos.length() > 0) {
                tableArtists = new ArrayList<Artist>();
            }
            for (int i = 0; i < datos.length(); i++) {
                JSONObject json_data = datos.getJSONObject(i);
                artist = new Artist();
                artist.setIdArtist(json_data.getInt(ID_ARTIST));
                artist.setName(json_data.getString(NAME));
                artist.setSurname(json_data.getString(SURNAME));
                artist.setNationality(json_data.getString(NATIONALITY));
                tableArtists.add(artist);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return tableArtists;

    }
}