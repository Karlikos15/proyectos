CREATE SEQUENCE SEQ_ID_ARTIST
START WITH 1
INCREMENT BY 1;

CREATE TABLE artist
(
	ID_ARTIST NUMBER(4),
	NAME VARCHAR2(50),
	SURNAME VARCHAR2(50),
	NATIONALITY VARCHAR2(50)
);

INSERT INTO artist (ID_ARTIST, NAME, SURNAME, NATIONALITY)
VALUES (SEQ_ID_ARTIST.NEXTVAL, 'Laura', 'Garcia', 'Espana');


CREATE OR REPLACE PACKAGE artists IS
TYPE vCursor IS REF CURSOR;

PROCEDURE existsArtist
(
	vName IN artist.NAME%TYPE,
	vSurname IN artist.SURNAME%TYPE,
	vNationality IN artist.NATIONALITY%TYPE,
	vUserCursor OUT vCursor
);

PROCEDURE findAll
(
	vUserCursor OUT vCursor
);

PROCEDURE insertArtist
(
	vName IN artist.NAME%TYPE,
	vSurname IN artist.SURNAME%TYPE,
	vNationality IN artist.NATIONALITY%TYPE,
	rowCount OUT INTEGER
);

PROCEDURE updateArtist
(
	vIdArtist IN artist.ID_ARTIST%TYPE,
	vName IN artist.NAME%TYPE,
	vSurname IN artist.SURNAME%TYPE,
	vNationality IN artist.NATIONALITY%TYPE
);

PROCEDURE deleteArtist
(
	vIdArtist IN artist.ID_ARTIST%TYPE
);

END artists;
/


CREATE OR REPLACE PACKAGE BODY artists IS
	PROCEDURE existsArtist
	(
		vName IN artist.NAME%TYPE,
		vSurname IN artist.SURNAME%TYPE,
		vNationality IN artist.NATIONALITY%TYPE,
		vUserCursor OUT vCursor
	) IS
	BEGIN
		OPEN vUserCursor FOR
			SELECT * FROM artist
			WHERE NAME = vName AND SURNAME = vSurname AND NATIONALITY = vNationality;
	END existsArtist;


	PROCEDURE findAll
	(
		vUserCursor OUT vCursor
	)IS
	BEGIN
		OPEN vUserCursor FOR
			SELECT * FROM artist;
	END findAll;


	PROCEDURE insertArtist
	(
		vName IN artist.NAME%TYPE,
		vSurname IN artist.SURNAME%TYPE,
		vNationality IN artist.NATIONALITY%TYPE,
		rowCount OUT INTEGER
	) IS
	BEGIN
		INSERT INTO artist VALUES (SEQ_ID_ARTIST.NEXTVAL, vName, vSurname, vNationality);
		rowCount:=SQL%ROWCOUNT;
	END insertArtist;


	PROCEDURE updateArtist
	(
		vIdArtist IN artist.ID_ARTIST%TYPE,
		vName IN artist.NAME%TYPE,
		vSurname IN artist.SURNAME%TYPE,
		vNationality IN artist.NATIONALITY%TYPE
	) IS
	BEGIN
		UPDATE artist SET NAME=vName, SURNAME=vSurname, NATIONALITY=vNationality
		WHERE ID_ARTIST=vIdArtist;
	END updateArtist;


	PROCEDURE deleteArtist
	(
		vIdArtist IN artist.ID_ARTIST%TYPE
	) IS
	BEGIN
		DELETE FROM artist WHERE ID_ARTIST=vIdArtist;
	END deleteArtist;
END artists;
/
