package com.svalero.musicapp.beans;


public class Single {
    private String artist;
    private int image;
    private String single;

    public Single(String single, int image, String artist) {
        this.single = single;
        this.image = image;
        this.artist = artist;
    }

    public Single() {
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getSingle() {
        return single;
    }

    public void setSingle(String single) {
        this.single = single;
    }
}
