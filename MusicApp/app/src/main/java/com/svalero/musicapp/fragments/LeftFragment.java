package com.svalero.musicapp.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.svalero.musicapp.R;
import com.svalero.musicapp.adapters.MyAdapterList;
import com.svalero.musicapp.beans.Single;
import com.svalero.musicapp.data.MusicAppData;

public class LeftFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    public LeftFragment() {}

    public static LeftFragment newInstance(Bundle params) {
        LeftFragment fragment = new LeftFragment();
        if(params!=null){
            fragment.setArguments(params);
        }
        return fragment;
    }
    public static LeftFragment newInstance() {
        return newInstance(null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnFragmentInteractionListener) context;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           // recoger parámetros que nos pase el Activity
        }
    }
    private ListView lstMusic;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View vista =
                inflater.inflate(R.layout.fragment_left,
                        container,
                        false);
        if(vista!=null){
            lstMusic = (ListView)
                    vista.findViewById(R.id.lstMusic);
        }
        return vista;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*Construir el Adapter*/

        MusicAppData.initLstSingle();
        lstMusic.setAdapter(new MyAdapterList(this, MusicAppData.getLstSingle()));

        /*Hace lo que dice el on item click listener*/
        lstMusic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent,
                                    View view,
                                    int position,
                                    long id) {
                mListener.onFragmentInteraction(MusicAppData.getLstSingle().get(position));
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Single single);
    };
}
