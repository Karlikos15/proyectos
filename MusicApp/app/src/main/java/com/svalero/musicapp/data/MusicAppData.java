package com.svalero.musicapp.data;

import com.svalero.musicapp.R;
import com.svalero.musicapp.beans.Single;
import java.util.ArrayList;


/**
 * Created by Carlos on 24/11/2016.
 */

public class MusicAppData {
    private static ArrayList<Single> lstSingle = new ArrayList();

    public static void initLstSingle(){
        lstSingle.add(new Single("Lana del Rey", R.drawable.lana_rey, "Cancion 1"));
        lstSingle.add(new Single("Adele", R.drawable.adele, "Cancion 2"));
        lstSingle.add(new Single("Mago Pis", R.drawable.lana_rey, "Cancion 3"));
    }

    public static ArrayList<Single> getLstSingle() {
        return lstSingle;
    }

    public static void setLstSingle(ArrayList<Single> lstSingle) {
        MusicAppData.lstSingle = lstSingle;
    }
}
