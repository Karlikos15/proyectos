package com.svalero.musicapp;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.svalero.musicapp.beans.Single;
import com.svalero.musicapp.fragments.LeftFragment;
import com.svalero.musicapp.fragments.RightTopFragment;

public class MainActivity extends AppCompatActivity implements LeftFragment.OnFragmentInteractionListener{
    private LeftFragment leftFragment;
    private RightTopFragment rightTopFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Gestor de Fragmentos

        FragmentManager fm = getSupportFragmentManager();

        //TRANSACCIÓN

        leftFragment = LeftFragment.newInstance();
        rightTopFragment = RightTopFragment.newInstance();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.contentLeft, leftFragment);
        ft.add(R.id.contentRightAbove, rightTopFragment);


        //COMMIT

        ft.commit();


    }

    @Override
    public void onFragmentInteraction(Single single){

        rightTopFragment.changeImageView(single);
    }
}
