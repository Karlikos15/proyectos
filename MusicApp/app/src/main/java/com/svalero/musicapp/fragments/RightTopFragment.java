package com.svalero.musicapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.svalero.musicapp.R;
import com.svalero.musicapp.beans.Single;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RightTopFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RightTopFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public RightTopFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RightTopFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RightTopFragment newInstance(String param1, String param2) {
        RightTopFragment fragment = new RightTopFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public static RightTopFragment newInstance(Bundle params) {
        RightTopFragment fragment = new RightTopFragment();
        if(params!=null){
            fragment.setArguments(params);
        }
        return fragment;
    }
    public static RightTopFragment newInstance() {
        return newInstance(null);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private ImageView imgCaratula;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View vista = inflater.inflate(R.layout.fragment_right_top, container, false);
        if (vista!=null){
            imgCaratula = (ImageView) vista.findViewById(R.id.imgCaratula);
        } return vista;
    }

    public void changeImageView(Single single){
        imgCaratula.setImageResource(single.getImage());
    }
}
