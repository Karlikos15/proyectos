package com.svalero.musicapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.svalero.musicapp.R;
import com.svalero.musicapp.beans.Single;
import com.svalero.musicapp.fragments.LeftFragment;

import java.util.ArrayList;
import java.util.List;


public class MyAdapterList extends ArrayAdapter<Single> {
    private ArrayList<Single> lstSingles;
    private LeftFragment leftFragment;
    public MyAdapterList(LeftFragment leftFragment, ArrayList<Single> objects) {
        super((Context)leftFragment.getActivity(),
                R.layout.item_list,
                objects);
        this.lstSingles = objects;
        this.leftFragment = leftFragment;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // INFLATERRRRRRR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        LayoutInflater inflater = leftFragment.getActivity().
                                getLayoutInflater();
        View item = inflater.inflate(R.layout.item_list, null);

        Single single = lstSingles.get(position);

    TextView txtArtist = (TextView) item.findViewById(R.id.txtArtist);
    TextView txtSingle = (TextView) item.findViewById(R.id.txtSingle);
        txtArtist.setText(single.getArtist());
        txtSingle.setText(single.getSingle());

        ImageView image = (ImageView) item.findViewById(R.id.imageView);
        image.setImageResource(single.getImage());

        return item;
    }
}
