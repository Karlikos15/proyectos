package motorSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MotorSQL {
    private Connection con;
    private Statement st;
    private ResultSet rs;
    private static final String URL = "jdbc:oracle:thin:system/system@localhost:1521:orcl";
    private static final String CONTROLADOR = "oracle.jdbc.OracleDriver";

    public void connect() {
        try {
            Class.forName(CONTROLADOR);
            con = DriverManager.getConnection(URL);
            st = con.createStatement();
        } catch (Exception ex) {
            System.out.println("Error al conectar a la base de datos.");
        }
    }

    public Connection getCon() {
        return this.con;
    }

    public void disconnect() {
        try {
            // ResultSet
            if (rs != null) {
                rs.close();
            }
            // Statement
            if (st != null) {
                st.close();
            }
            // Connection
            if (con != null) {
                con.close();
            }
        } catch (Exception ex) {
            System.out.println("Error al cerrar la conexión. ");
        }
    }

    public int execute(String sql) {
        int resp = 0;
        try {
            resp = st.executeUpdate(sql);
        } catch (Exception ex) {
            System.out.println("Error al modificar los datos. ");
        }
        return resp;
    }

    public ResultSet executeQuery(String sql) {
        try {
            rs = st.executeQuery(sql);
        } catch (Exception ex) {
            System.out.println("Error al consultar los datos. ");
        }
        return rs;
    }
}
