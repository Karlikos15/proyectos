package beans;

import java.util.ArrayList;

public class Artist {

    private int idArtist;
    private String name;
    private String surname;
    private String nationality;

    public Artist(int idArtist, String name, String surname, String nationality) {
        this.idArtist = idArtist;
        this.name = name;
        this.surname = surname;
        this.nationality = nationality;
    }

    public Artist(String name, String surname, String nationality) {
        this.name = name;
        this.surname = surname;
        this.nationality = nationality;
    }

    public Artist() {
    }

    public int getIdArtist() {
        return idArtist;
    }

    public void setIdArtist(int idArtist) {
        this.idArtist = idArtist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return "Artist{" + "idArtist=" + idArtist + ", name=" + name + ", surname=" + surname + ", nationality=" + nationality + '}';
    }

    public static String toArrayJSON(ArrayList<Artist> listArtist) {
        String resp = "";
        int i = 0;
        resp += "[";
        for (Artist artist : listArtist) {
            resp += "{";
            resp += "'ID_ARTIST':";
            resp += artist.getIdArtist();
            resp += ",";
            resp += "'NAME':";
            resp += "'" + artist.getName() + "'";
            resp += ",";
            resp += "'SURNAME':";
            resp += "'" + artist.getSurname() + "'";
            resp += ",";
            resp += "'NATIONALITY':";
            resp += "'" + artist.getNationality() + "'";
            resp += "}";
            resp += (i + 1 == listArtist.size() ? "" : ",");
            i++;
        }
        resp += "]";
        
        return resp;
    }
}
