package controller;

import action.Action;
import beans.Artist;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import motorSQL.MotorSQL;
import serviceDAO.ServiceArtistDAO;

public class ArtistAction implements Action {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String pagDestino = "";
        String action = (String) request.getParameter("ACTION");
        String[] arrayAction = action.split("\\.");
        if (arrayAction[1].equals("ADD")) {
            add(request, response);
        } else if (arrayAction[1].equals("DELETE")) {
            //pagDestino = delete(request, response);
        } else if (arrayAction[1].equals("UPDATE")) {
            //pagDestino= update(request, response);
        } else if (arrayAction[1].equals("LIST")) {
            pagDestino = all(request, response);
        } else if (arrayAction[1].equals("FILTER")) {

        }

        return pagDestino;
    }

    private void add(HttpServletRequest request, HttpServletResponse response) {
        String name = (String) request.getParameter("NAME");
        String surname = (String) request.getParameter("SURNAME");
        String nationality = (String) request.getParameter("NATIONALITY");

        Artist artist = new Artist();
        artist.setName(name);
        artist.setSurname(surname);
        artist.setNationality(nationality);

        ServiceArtistDAO dao = new ServiceArtistDAO(new MotorSQL());

        try {
            dao.insertArtist(artist);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("ID_ARTIST"));

        Artist artist = new Artist();
        artist.setIdArtist(id);

        ServiceArtistDAO dao = new ServiceArtistDAO(new MotorSQL());

        try {
            dao.deleteArtist(artist);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    private void update(HttpServletRequest request, HttpServletResponse response) {
        int id = Integer.parseInt(request.getParameter("ID_ARTIST"));
        String name = (String) request.getParameter("NAME");
        String surname = (String) request.getParameter("SURNAME");
        String nationality = (String) request.getParameter("NATIONALITY");

        Artist artist = new Artist();
        artist.setIdArtist(id);
        artist.setName(name);
        artist.setSurname(surname);
        artist.setNationality(nationality);

        ServiceArtistDAO dao = new ServiceArtistDAO(new MotorSQL());

        try {
            dao.updateArtist(artist);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    private String all(HttpServletRequest request, HttpServletResponse response) {
        ServiceArtistDAO dao = new ServiceArtistDAO(new MotorSQL());

        Artist artist = new Artist();

        ArrayList<Artist> lstArtists = null;
        
        try{
            lstArtists=dao.all(artist);
        }catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        System.out.println(Artist.toArrayJSON(lstArtists));
        return Artist.toArrayJSON(lstArtists);
    }
}
