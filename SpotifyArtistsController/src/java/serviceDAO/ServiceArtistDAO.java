package serviceDAO;

import beans.Artist;
import java.util.ArrayList;
import motorSQL.MotorSQL;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import oracle.jdbc.OracleTypes;

public class ServiceArtistDAO {

    private MotorSQL miConexion;

    public ServiceArtistDAO(MotorSQL miConexion) {
        this.miConexion = miConexion;
    }

    public int insertArtist(Artist artist) throws Exception {
        int conteoFilas = 0;

        this.miConexion.connect();
        Connection con = this.miConexion.getCon();
        CallableStatement cs;
        ResultSet rs;

        cs = con.prepareCall("{CALL artists.insertArtist(?, ?, ?, ?)}");

        cs.setString(1, artist.getName());
        cs.setString(2, artist.getSurname());
        cs.setString(3, artist.getNationality());
        cs.registerOutParameter(4, OracleTypes.INTEGER);

        cs.execute();

        this.miConexion.disconnect();

        conteoFilas = (int) cs.getObject(3);

        return conteoFilas;
    }

    public void deleteArtist(Artist artist) throws Exception {
        this.miConexion.connect();
        Connection con = this.miConexion.getCon();
        CallableStatement cs;
        ResultSet rs;

        cs = con.prepareCall("{CALL artists.deleteArtist(?)}");

        cs.setInt(1, artist.getIdArtist());

        cs.execute();

        this.miConexion.disconnect();
    }

    public void updateArtist(Artist artist) throws Exception {
        this.miConexion.connect();
        Connection con = this.miConexion.getCon();
        CallableStatement cs;
        ResultSet rs;

        cs = con.prepareCall("{CALL artists.updateArtist(?, ?, ?, ?)}");

        cs.setInt(1, artist.getIdArtist());
        cs.setString(2, artist.getName());
        cs.setString(3, artist.getSurname());
        cs.setString(4, artist.getNationality());

        cs.execute();

        this.miConexion.disconnect();
    }

    public ArrayList<Artist> all(Artist artist) throws Exception {
        this.miConexion.connect();
        Connection con = this.miConexion.getCon();
        CallableStatement cs;
        ResultSet rs;

        cs = con.prepareCall("{CALL artists.findAll(?)}");

        cs.registerOutParameter(1, OracleTypes.CURSOR);

        cs.execute();

        rs = (ResultSet) cs.getObject(1);

        ArrayList<Artist> tabla = new ArrayList();
        while (rs.next()) {
            Artist myArtist = new Artist();
            myArtist.setIdArtist(rs.getInt(1));
            myArtist.setName(rs.getString(2));
            myArtist.setSurname(rs.getString(3));
            myArtist.setNationality(rs.getString(4));
            tabla.add(myArtist);
        }
        this.miConexion.disconnect();
        return tabla;
    }

    public ArrayList<Artist> filter(Artist artist) throws Exception {
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        //NO FUNCIONA DE MOMENTO
        this.miConexion.connect();
        Connection con = this.miConexion.getCon();
        CallableStatement cs;
        ResultSet rs;
        
        cs = con.prepareCall("{CALL artists.filter(?)}");

        cs.registerOutParameter(1, OracleTypes.CURSOR);

        cs.execute();

        rs = (ResultSet) cs.getObject(1);

        ArrayList<Artist> tabla = new ArrayList();
        while (rs.next()) {
            Artist myArtist = new Artist();
            myArtist.setIdArtist(rs.getInt(1));
            myArtist.setName(rs.getString(2));
            myArtist.setSurname(rs.getString(3));
            myArtist.setNationality(rs.getString(3));
            tabla.add(myArtist);
        }
         
        this.miConexion.disconnect();

        return tabla;
    }
}
